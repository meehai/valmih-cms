<?php
/**
 * Connection.php
 *
 * A core class that defines a connection to the database. It offers an API to operate the database: connect/disconnect
 * from the database, perform queries, extract information in an easy way, etc.
 */
/* TODO -> API pentru querys */
/* TODO - creare baze de date cu elemente text de dimensiune fixa la elementele unde se stie dimensiunea (de exemplu, campul unde este parola MD5 stim de la inceput ca are 32 caractere) */

abstract class Connection {
	/* Main connection object that must be overwritten by various implementation (i.e. MySQLiConnection) */
	protected $connection;

	abstract static function createDB();
	abstract static function checkDB();
	abstract static function deleteDB();
	abstract function createTable($name, $arrNames, $types, $firstRowAutoIncrement=true, $firstRowPrimaryKey=true);
	abstract function query($queryStr);
	abstract function close();
	abstract function getError();
	abstract function getLastInsertedId();

	static function installModules($connection) {
		/* Get all the modules from the $_POST array */
		$modules = [];
		foreach ($_POST as $key=>$item) {
			if (startsWith($key, "checkBoxModule_")) {
				$modules[] = $item;
			}
		}

		foreach ($modules as $module) {
			try {
				/* Load the module and call its install method. If it doesn't throw any error, the module will be added
				 *  to the list of installed modules in th DB. The module must have a file and a class named
				 *  identically as the directory name. */
				$modulePath = Constants::$modulesPath . "/$module";
				require_once("$modulePath/$module.php");
				(new $module($connection, $modulePath))->install();
				$connection->insertInto("settings", ["category", "name", "information"],
					["modules", $module, "active"]);
			}
			catch (Exception $e) {
				echo "Error installing module $module with error $e<br/>";
				throw $e;
			}
		}

		return true;
	}

	static function createInitialTables($connection) {
		$connection->createTable("settings", ["id", "category", "name", "value", "information"],
			["integer", "varchar(50)", "varchar(50)", "varchar(100)", "varchar(256)"]);
		$connection->createTable("users", ["id", "username", "password"],
			["integer", "varchar(32)", "varchar(32)"]);

		/* Site settings */
		$footerMessage = "All rights reserved © valmih CMS " . date("Y");
		$connection->insertInto("settings", ["category", "value"], ["footerMessage", $footerMessage]);
		$connection->insertInto("settings", ["category", "value"], ["siteTheme", $_POST["selectedTheme"]]);
		$connection->insertInto("settings", ["category", "value"], ["routesPath", "application/routes.txt"]);

		/* Insert admin account*/
		$connection->insertInto("users", ["username", "password"], [$_POST["adminUsername"], $_POST["adminPassword"]]);
		Connection::installModules($connection);
	}

	/*
		Nota: functie generala de extras informatie
		Adoptia ei este nefortata pastrand-o pe cea veche si modificand in timp vechiul apel cu noul, apoi stergand-o pe vechea si facand replace spre viitorul nou nume.
		Primul parametru:		coloanele ce trebuie extrase din tabela cautata
		Al doilea parametru:	tabela in care se cauta informatiile
		Al treilea parametru:	criteriul dupa care se extrag informatiile (inclusiv ORDER BY)
		Al patrulea parametru:	format special, de exemplu "count", "first_row", "value", "first_value"

		Se verifica daca dimensiunea tabelei returnate cuprinde mai mult de o linie.
		Daca da, atunci rezultatul este doar linia, altfel rezultatul este o tabela asociativa.

		Exemple documentate: docs/getInfoExamples.txt
		Exemplu folosire:	getInfo("permisiuni", "users", "email='$email'"); 	// este un camp unic -> se intoarce doar linia
							getInfo("*", "anunturi", "id=$id");					// se intoarce tabela cuprinzand anunturile utilizatorului cu id-ul $id
							getInfo("nume, prenume", "users", "id=$id", "first_row");		// intoarce o linie din tabela de useri formata doar din coloanele nume si prenume
							getInfo("nume", "prenume', "users", "true"); 		// intoarce o tabela cu toti userii cuprinzand doar coloanele nume si prenume
							getInfo(NULL, "anunturi", "activ=1 AND acceptat=1", "count"); 	// intoarce numarul de linii din tabela anunturi care au campurile activ = 1 si acceptat = 1
							getInfo("*", "setari", "identificator='meniu'"); 	// intoarce tabela cuprinzand toate coloanele din tabela setari unde identificatorul este 'meniu'

		TODO - de facut un review al functiei
	*/
	/* @brief SELECT++ function for DB management. It should read like:
	 *  SELECT <rows> from <table> where <criteria>
	 * @param[in] rows The rows to be extracted
	 * @param[in] table The table from which data is extracted
	 * @param[in] criteria The criteria based on which data is extracted
	 * @param[in] info Various other information (TODO details)
	 * @return A value or an array with the desired data from the database. I
	 * */
	function getInfo($rows, $table, $criteria = "true", $info = "") {
        /* Changing from "first_row, first_value" => array ("first_row", "first_value") */
		$info = strToArray($info);

		/* $info = "count, whatever_else" will always take care of the count part */
		if(count($info) > 0 && $info[0] == "count") {
            assert (count($info) == 1);
            $query = "SELECT COUNT(*) FROM $table WHERE $criteria";
			$result = $this->query($query);
			assert ($result, "Error retrieving data from DB using query: $query");

			$a = $result->fetchArray("num")[0];
			return $a[0];
		}

		$returnedArray = array();
		$query = "SELECT $rows FROM $table WHERE $criteria";
		$result = $this->query($query);
		assert ($result, "Error retrieving data from DB using query: $query");

		$returnedArray = $result->fetchArray("assoc");

		/* If empty array, just return it as is without further processing. */
		if(!isset($returnedArray[0])) {
			return [];
		}

		/* Cazul clasic => intorc un array([0] => array( "a" => "b", ...), [1] => ..., ) */
		if(count($info) == 0) {
            return $returnedArray;
        }

        /* array([0] => array ("a" => "b", ...), [1] => ... ) ==> array([0] => array("b", ...), [1] => ...) */
        if(in_array("value", $info)){
            $n = count($returnedArray);
            for($i=0; $i<$n; $i++){
                $newArr = array();
                foreach ($returnedArray[$i] as $key => $value)
                $newArr[] = $value;

                $returnedArray[$i] = $newArr;
            }
        }
        /* array([0] => array ("a" => "b", ...), [1] => ... ) ==> array([0] => "b", [1] => ...) */
        else if(in_array("first_value", $info)){
            $n = count($returnedArray);
            /* "col1, col2, col4" => array("col1", "col2", "col4") */
            $arrayedRows = strToArray($rows);

            for($i=0; $i<$n; $i++){
                $returnedArray[$i] = $returnedArray[$i][$arrayedRows[0]];
            }
        }

		if(in_array("first_row", $info) || in_array("first", $info) || in_array("first_result", $info))
			$returnedArray = $returnedArray[0];

		return $returnedArray;
	}

	function insertInto($table, $columns, $values, $condition=1) {
		$columns = strToArray($columns);
		$values = strToArray($values);
		$nColumns = count($columns);
		assert ($nColumns == count($values) && $nColumns > 0);

		$qColumns = "`$columns[0]`";
		$qValues = "'$values[0]'";
		for($i=1; $i<$nColumns; $i++) {
			$qColumns .= ", `$columns[$i]`";
			$qValues .= ", '$values[$i]'";
		}

		$query = "INSERT INTO `$table` ($qColumns) VALUES ($qValues)";
		$this->query($query);
	}

	function updateTable($table, $columns, $values, $condition=1) {
		$columns = strToArray($columns);
		$values = strToArray($values);

		$nColumns = count($columns);
		assert ($nColumns == count($values) && $nColumns > 0);

		$query = "UPDATE `$table` SET ";
		$query .= "`$columns[0]`='$values[0]'";
		for($i=1; $i<$nColumns; $i++) {
			$query .= ", `$columns[$i]`='$values[$i]'";
		}
		$query .= " WHERE $condition";
		$this->query($query);
	}

	function getUserID($username) {
		$userID = $this->getInfo("id", "users", "username='$username'", "first_row,first_value");
		assert (!$userID == NULL, "Fail for username $username");
		return $userID;
	}

}
?>
