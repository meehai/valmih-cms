<?php

class login extends Module {
	public static $maxLoggedTime = 60 * 60 * 24 * 7;
	function __construct($connection, $modulePath) {
		parent::__construct($connection, $modulePath);
		$this->checkSessionExpiration();
	}

	function install() {}

	function loginCondition($user, $password) {
		$item = $this->connection->getInfo("username,password", "users", "username='$user'");
		if(!count($item) == 1) {
			return false;
		}

		/* TODO: Don't do plain text password check :)*/
		if(!($item[0]["password"] === $password)) {
			return false;
		}

		return true;
	}

	function tryLogin($user, $password) {
		assert (!$this->isLogged());
		assert (isset($user));
		assert (isset($password));

		if (!$this->loginCondition($user, $password)) {
			return false;
		}

		$_SESSION["user"] = $user;
		$_SESSION["user_logging_time"] = time();
		return true;
	}

	function isLogged() {
		return isset($_SESSION["user"]);
	}

	function checkSessionExpiration() {
		if(!$this->isLogged()) {
			return;
		}

		$timeSinceLogin = $this->getTimeSinceLogin();
		/* Log user out every 7 days for security. */
		if($timeSinceLogin > login::$maxLoggedTime) {
			$this->logout();
		}
	}

	function logout() {
		assert ($this->isLogged());
		unset($_SESSION["user"]);
		unset($_SESSION["user_logging_time"]);
	}

	function getTimeSinceLogin() {
		assert ($this->isLogged());
		return time() - $_SESSION["user_logging_time"];
	}

	function getUserId() {
		assert ($this->isLogged());
		$item = $this->connection->getInfo("id", "users", "username='$_SESSION[user]'", "first_row,first_value");
		return $item;
	}

}

?>