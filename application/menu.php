<?php
$menuModule = Page::getInstance()->getModule("main_menu");
$loginModule = Page::getInstance()->getModule("login");
$menuArray = $menuModule->getMenuItems(Constants::$applicationPath . "/menu.json");
$webPath = Constants::$webPath;

$loginWebPath = $loginModule->isLogged() ? "$webPath/login?logout" : "$webPath/login";
$loginText = $loginModule->isLogged() ? "Logout" : "Login";

$menuStr = "<ul class=\"level1\">";
foreach($menuArray as $menuItem) {
	$menuStr .= "<li>";
	if (isset($menuItem["href"])) {
		$menuStr .= "<a href=$menuItem[href]> $menuItem[name] </a>";
	}
	else {
		$menuStr .= "$menuItem[name]";
	}
	$menuStr .= "</li>";
	$menuStr .= "<ul class=\"level2\">";
	foreach($menuItem["children"] as $childrenItem) {
		$menuStr .= "<li> <a href=$childrenItem[href]> $childrenItem[name] </a> </li>";
	}
	$menuStr .= "</ul>";
}
$menuStr .= "<ul>";

echo <<<EOF
<div class="menuBox">
	<div class="menuBoxTitle">
		<a href="$webPath" class="logoText"> Meehai </a>
		<a href="$loginWebPath" class="loginText"> $loginText </a>
	</div>
	<br/>
	$menuStr
</div>
EOF;
